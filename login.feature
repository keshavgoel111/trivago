
@run
Feature: MagazinePage

  Scenario: Search for any location on Magazine by using the search bar
    Given Open https://magazine.trivago.com/
    When  Search for Asia location on Magazine by using the search bar
    Then Results should be filtered based on search criteria

  Scenario: Fill in the contact form and send it (accessible through the footer)
    Given Click on Contact Link button on Home Page
    When Fill in the required fields and submit the details
    Then Successfully Sumbitted Message should be displayed

  Scenario: Subscribe to the Newsletter
    Given Enter Email Id Newsletter Subscription Box
    When Click on the Inspire Me Button
    Then Successfully Subscribed Message should be displayed


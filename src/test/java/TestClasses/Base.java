package TestClasses;

import Utils.Browser;
import Utils.WaitUtil;
import dataProvider.ConfigFileReader;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

public class Base {

    public static WebDriver driver;
    public static WaitUtil waitUtil = new WaitUtil();
    public static ConfigFileReader configFileReader = new ConfigFileReader();

    @BeforeSuite public void beforeSuite()
    {
        driver=invokeBrowser();
    }

    private WebDriver invokeBrowser()
    {
        System.out.println("In invokeBrowser method");
        if(configFileReader.getEnvironment().equalsIgnoreCase("Mac OS X")&&configFileReader.getBrowser().equalsIgnoreCase("Firefox"))
        {
            driver=Browser.InvokeBrowser(configFileReader.getBrowser());
        }
        if(configFileReader.getEnvironment().equalsIgnoreCase("Linux")&&configFileReader.getBrowser().equalsIgnoreCase("Firefox"))
        {
            driver=Browser.InvokeBrowser(configFileReader.getBrowser());
        }
        if(configFileReader.getEnvironment().equalsIgnoreCase("Mac OS X")&&configFileReader.getBrowser().equalsIgnoreCase("Chrome"))
        {
            driver=Browser.InvokeBrowser(configFileReader.getBrowser());
        }
        if(configFileReader.getEnvironment().equalsIgnoreCase("Mac OS X")&&configFileReader.getBrowser().equalsIgnoreCase("Chrome Canary"))
        {
            driver= Browser.InvokeBrowser(configFileReader.getBrowser());
        }
        return driver;
    }

    @AfterSuite(alwaysRun = true) public void afterSuite()
    {
        //        try
        //        {
        //            System.out.println("Sending Mail");
        //             EmailReport.sendMail();
        //        }
        //        catch (FileNotFoundException e)
        //        {
        //            e.printStackTrace();
        //        }
        //        catch (IOException e)
        //        {
        //            e.printStackTrace();
        //        }
        System.out.println("Closing Driver");
        driver.close();
    }
}
package DataBaseConnection;

import dataProvider.ConfigFileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DataBaseConnection  {

    private static String dbusername;
    private static String dbpassword;
    static ConfigFileReader configFileReader=new ConfigFileReader();
    //Should be defined as jdbc:mysql://host:port/database name
    //private static String databaseURLQA= "";
    //private static String databaseURLPRODUCTION= "";

    public static String executeSQLQuery(String testEnv, String sqlQuery) {

        System.out.println("In executeSQLQuery Method");
        String databaseURL=configFileReader.getDBUrl(testEnv);
        System.out.println("databaseURL="+databaseURL);
        String connectionUrl="";
        Connection connection;

        //To connect with QA Database
        if(testEnv.equalsIgnoreCase("QA")){
            connectionUrl = databaseURL;
            dbusername = "";
            dbpassword = "";
        }
        //To connect with Stage Database
        else if(testEnv.equalsIgnoreCase("STAGE")) {
            System.out.println("Test Environment Found:STAGE");
            connectionUrl = databaseURL;
            dbusername = "";
            dbpassword = "";
        }

        //To connect with Production Database
        else if(testEnv.equalsIgnoreCase("PRODUCTION")) {
            connectionUrl = databaseURL;
            dbusername = "";
            dbpassword = "";
        }
        try {
            Class.forName("com.mysql.jdbc.Driver");
        }catch(ClassNotFoundException e) {
            e.printStackTrace();
        }

        String resultValue = "";
        try {
            connection = DriverManager.getConnection(connectionUrl,dbusername,dbpassword);
            System.out.println(connection);
            if(connection!=null) {
                System.out.println("Connected to the database...");
            }else {
                System.out.println("Database connection failed to "+testEnv+" Environment");
            }
            Statement stmt = connection.createStatement();
            System.out.println("Executing Sql Query");
            System.out.println(sqlQuery);
            ResultSet rs = stmt.executeQuery(sqlQuery);
            System.out.println("rs is:"+rs);
            try {
                System.out.println("In Try block");
                System.out.println(rs.next());
                while(rs.next()){
                    System.out.println("Sql query executed successfully. Printing Result....");
                    resultValue = rs.getString(1).toString();
                    System.out.println(resultValue);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            catch (NullPointerException err) {
                System.out.println("No Records obtained for this specific query");
                err.printStackTrace();
            }
            System.out.println("Closing Sql Connection");
            connection.close();
            System.out.println("Database Disconnected");

        }catch(SQLException sqlEx) {
            System.out.println( "SQL Exception:" +sqlEx.getStackTrace());
        }
        return resultValue;
    }


    public static ArrayList<String> executeSQLQuery_List(String testEnv, String sqlQuery) {
        String databaseURL=configFileReader.getDBUrl(testEnv);
        String connectionUrl="";
        Connection connection;
        ArrayList<String> resultValue = new ArrayList<String>();
        ResultSet resultSet;

        //To connect with QA Database
        if(testEnv.equalsIgnoreCase("QA")){
            connectionUrl = databaseURL;
            dbusername = "";
            dbpassword = "";
        }

        //To connect with Stage Database
        else if(testEnv.equalsIgnoreCase("STAGE")) {
            connectionUrl = databaseURL;
            dbusername = "";
            dbpassword = "";
        }

        //To connect with Production Database
        else if(testEnv.equalsIgnoreCase("PRODUCTION")) {
            connectionUrl = databaseURL;
            dbusername = "";
            dbpassword = "";
        }

        try {
            Class.forName("com.mysql.jdbc.Driver");
        }catch(ClassNotFoundException e) {
            e.printStackTrace();
        }

        try {
            connection = DriverManager.getConnection(connectionUrl,dbusername,dbpassword);
            if(connection!=null) {
                System.out.println("Connected to the database");
            }else {
                System.out.println("Failed to connect to "+testEnv+" database");
            }
            Statement statement = connection.createStatement();
            resultSet=statement.executeQuery(sqlQuery);

            try {
                while(resultSet.next()){
                    int columnCount = resultSet.getMetaData().getColumnCount();
                    StringBuilder stringBuilder = new StringBuilder();
                    for(int iCounter=1;iCounter<=columnCount; iCounter++){
                        stringBuilder.append(resultSet.getString(iCounter).trim()+" ");
                    }
                    String reqValue = stringBuilder.substring(0, stringBuilder.length()-1);
                    resultValue.add(reqValue);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            catch (NullPointerException ex) {
                System.out.println("No Records found for this specific query" +ex.getStackTrace());
            }
            finally {
                if (connection != null) {
                    try {
                        connection.close();
                    } catch (SQLException ex) {
                        System.out.println( "SQL Exception:" +ex.getStackTrace());
                    }
                }
            }

        }catch(SQLException sqlEx) {
            System.out.println( "SQL Exception:" +sqlEx.getStackTrace());
        }
        return resultValue;
    }
}

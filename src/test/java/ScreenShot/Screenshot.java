package ScreenShot;

import java.io.File;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class Screenshot {
	public static void captureScreenshot(WebDriver driver,String screenshotName)
	{
		try
		{
			System.out.println("In ScreenShot Method");
			System.out.println(driver);
			System.out.println(screenshotName);
			TakesScreenshot takesScreenshot=(TakesScreenshot)driver;
			System.out.println(takesScreenshot);
			File source=takesScreenshot.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(source, new File("./Screenshots/"+screenshotName+".png"));
			System.out.println("Screenshot Taken"+screenshotName);
		}
		catch (Exception e)
		{
			System.out.println("Exception while taking screenshot "+e.getMessage());
		}
	}
}

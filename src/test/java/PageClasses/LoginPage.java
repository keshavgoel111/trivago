package PageClasses;

import DataBaseConnection.DataBaseConnection;
import ScreenShot.Screenshot;
import Utils.WaitUtil;
import dataProvider.ConfigFileReader;
import java.util.ArrayList;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {

    WebDriver driver;
    WaitUtil wait;
    ConfigFileReader configFileReader;
    DataBaseConnection dataBaseConnection;
    Screenshot screenshot;

    public LoginPage(WebDriver driver,WaitUtil wait,ConfigFileReader configFileReader)
    {
        System.out.println(driver);
        this.driver=driver;
        this.wait=wait;
        this.configFileReader=configFileReader;
    }

    public void Trivago()
    {
        By SearchButton = By.xpath("//*[@id=\"header\"]/div/div/div[2]");
        By SearchBox = By.xpath("//*[@id=\"search\"]/div[1]/div[2]/input");
        By Contact = By.xpath("//*[@id=\"footer-main\"]/div[1]/div[3]/div[1]/div[2]/a");
        By ContactSubmit = By.xpath("//*[@id=\"app\"]/div[3]/div/div/div[1]/button");
        By ContactMessage = By.xpath("//*[@id=\"app\"]/div[3]/div/div/div[1]/div[2]/div/textarea");
        By ContactName = By.xpath("//*[@id=\"app\"]/div[3]/div/div/div[1]/div[3]/div[1]/input");
        By ContactEmail = By.xpath("//*[@id=\"contact-email\"]");
        By ContactTermsCheckBox = By.xpath("//*[@id=\"confirm\"]");
        By ContactMessageSent = By.xpath("//*[@id=\"app\"]/div[3]/div/div/div[1]/div/p");
        By TrivagoHome = By.xpath("//*[@id=\"header\"]/div/div/a");
        By NewsletterEmailBox = By.xpath("//*[@id=\"app\"]/div[3]/div/div/div[4]/section/div/div/div[2]/div/div[3]/div[2]/div[1]/input");
        By NewsletterSubmit = By.xpath("//*[@id=\"app\"]/div[3]/div/div/div[4]/section/div/div/div[2]/div/div[3]/div[2]/div[2]/button");
        By NewsletterSubmitResponse = By.xpath("//*[@id=\"app\"]/div[3]/div/div/div[3]/section/div/p");


        WebDriverWait wait = new WebDriverWait(driver, 30);
        try {
            Thread.sleep(1000);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        wait.until(ExpectedConditions.visibilityOfElementLocated(SearchButton));
        driver.findElement(SearchButton).click();
        try {
            Thread.sleep(1000);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(SearchBox).sendKeys("Miami");
        driver.findElement(SearchBox).sendKeys(Keys.ENTER);
        try {
            Thread.sleep(1000);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        String Text = driver.findElement(By.xpath("//*[@id=\"search\"]/div[1]/div[8]/h3")).getText();
        System.out.println("Number of Results=" + Text);
        driver.navigate().to("https://magazine.trivago.com/");
        wait.until(ExpectedConditions.visibilityOfElementLocated(Contact));
        driver.findElement(Contact).click();
        driver.manage().window().fullscreen();
        ArrayList<String> Available_tabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(Available_tabs.get(1));
        wait.until(ExpectedConditions.visibilityOfElementLocated(ContactSubmit));
        driver.findElement(ContactMessage).sendKeys("Test");
        driver.findElement(ContactName).sendKeys("Keshav");
        driver.findElement(ContactEmail).sendKeys("test@gmail.com");
        driver.findElement(ContactTermsCheckBox).click();
        driver.findElement(ContactSubmit).click();
        try {
            Thread.sleep(1000);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        wait.until(ExpectedConditions.visibilityOfElementLocated(ContactMessageSent));
        String Msg = driver.findElement(ContactMessageSent).getText();
        System.out.println(Msg);
        driver.close();
        driver.switchTo().window(Available_tabs.get(0));
        //driver.findElement(TrivagoHome).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(NewsletterEmailBox));
        driver.findElement(NewsletterEmailBox).sendKeys("test@gmail.com");
        wait.until(ExpectedConditions.visibilityOfElementLocated(NewsletterSubmit));
        driver.findElement(NewsletterSubmit).click();
        WebElement Submit = driver.findElement(By.xpath("//*[@id=\"app\"]/div[3]/div/div/div[4]/section/div/div/div[2]/div/div[3]/div[1]/div[1]"));
        if (!(Submit == null)) {
            System.out.println("Email Id is invalid");
        }
        else {
            wait.until(ExpectedConditions.visibilityOfElementLocated(NewsletterSubmitResponse));
            String Response = driver.findElement(NewsletterSubmitResponse).getText();
            System.out.println(Response);
        }
    }
    }

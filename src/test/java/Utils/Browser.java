package Utils;

import TestClasses.Base;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

public class Browser extends Base {
    static WebDriver driver;

    public static WebDriver InvokeBrowser(String browser)
    {
        System.out.println("In Browser Utils");
        if(browser.equalsIgnoreCase("Firefox"))
        {
            /*System.setProperty("webdriver.firefox.marionette","/Users/admin/Downloads/geckodriver2");            driver=new FirefoxDriver();
            System.out.println("Firefox Driver Invoked");
            final FirefoxOptions firefoxOptions = new FirefoxOptions();
            final FirefoxBinary firefoxBinary = new FirefoxBinary();
             FirefoxProfile profile = new FirefoxProfile();
            driver=new FirefoxDriver();
            driver.manage().window().maximize();
            System.out.println("driver invoked");
            firefoxOptions.setBinary(firefoxBinary);
            firefoxOptions.setProfile(profile);
            driver=new FirefoxDriver(firefoxOptions);
            System.out.println(driver);*/
        }
        else if (browser.equalsIgnoreCase("Chrome"))
        {
            System.out.println(configFileReader.getEnvironment());
            System.setProperty("webdriver.chrome.driver","/usr/local/chromedriver");
            driver=new ChromeDriver();
            System.out.println("Chrome Driver Invoked");
            System.out.println(configFileReader.getApplicationUrl());
            driver.get(configFileReader.getApplicationUrl());
            System.out.println("Opening : "+configFileReader.getApplicationUrl());
        }
        else if (browser.equalsIgnoreCase("Chrome Canary"))
        {

            System.out.println(configFileReader.getEnvironment());
            System.setProperty("webdriver.chrome.driver", "/usr/local/chromedriver");
            System.out.println("in Chrome Canary");
            DesiredCapabilities capabilities = DesiredCapabilities.chrome();
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.setBinary("/Applications/Google Chrome Canary.app/Contents/MacOS/Google Chrome Canary");
            chromeOptions.addArguments("test-type");
            chromeOptions.addArguments("--headless");
            chromeOptions.addArguments("--window-size=1920,1080");
            chromeOptions.addArguments("--disable-gpu");
            chromeOptions.addArguments("--proxy-server='direct://'");
            chromeOptions.addArguments("--proxy-bypass-list=*");
            chromeOptions.addArguments("--start-maximized");
            driver = new ChromeDriver(chromeOptions);
            driver.manage().window().setSize(new Dimension(1800, 1080));
            System.out.println(driver);
            System.out.println(configFileReader.getApplicationUrl());
            driver.get(configFileReader.getApplicationUrl());
            System.out.println("Opening : "+configFileReader.getApplicationUrl());
        }
        System.out.println("Returning driver:"+driver);
            return driver;
    }
    public WebDriver maximizeBrowser(WebDriver driver) {
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
        return driver;

    }
}


package Utils;

import com.google.common.base.Function;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class WaitUtil {
    public WebElement fluentWait(WebDriver driver,final By locator)
    {
        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(50, TimeUnit.SECONDS).pollingEvery(5, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);
        WebElement webElement = wait.until(new Function<WebDriver, WebElement>()
        {
            public WebElement apply(WebDriver driver)
            {
                return driver.findElement(locator);
            }});
        return webElement;
    };

    public WebElement fluentWaitForElementTextLength(WebDriver driver,final By locator)
    {
        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(60, TimeUnit.SECONDS).pollingEvery(5, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);
        WebElement webElement= wait.until(new Function<WebDriver, WebElement>()
        {
            public WebElement apply(WebDriver driver)
            {
                if(driver.findElement(locator).getText().length()>0)
                    return driver.findElement(locator);
                return driver.findElement(By.id("invalid"));


            }});
        return webElement;
    };

    public static void sleep(long milliseconds)
    {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static WebDriverWait explicitWaitByVisibilityOfElement(WebDriver driver, int seconds,final By locator){
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        //wait.until(ExpectedConditions.visibilityOf(el));
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(locator));
        return wait;
    }
    public void PageLoadWait(WebDriver driver){
        ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete");
                    }
                };
        try {

            WebDriverWait wait = new WebDriverWait(driver, 30);
            wait.until(expectation);
            System.out.println("page load complete");
        } catch (Throwable error) {
            Assert.fail("Timeout waiting for Page Load Request to complete.");
        }
    }
}


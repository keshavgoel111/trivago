/*
package Reporter;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import org.fit.cssbox.css.CSSNorm;
import org.fit.cssbox.css.DOMAnalyzer;
import org.fit.cssbox.css.NormalOutput;
import org.fit.cssbox.css.Output;
import org.fit.cssbox.io.DefaultDOMSource;
import org.fit.cssbox.io.DefaultDocumentSource;
import org.fit.cssbox.io.DocumentSource;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class inlineCSS {
	
	public static void ConvertTestNGReport(String path) throws IOException, SAXException{
		 DocumentSource docSource = new DefaultDocumentSource(path);

	      //Parse the input document
	      DefaultDOMSource parser = new DefaultDOMSource(docSource);
	      Document doc = parser.parse();

	      //Create the CSS analyzer
	      DOMAnalyzer da = new DOMAnalyzer(doc, docSource.getURL());
	      da.attributesToStyles(); //convert the HTML presentation attributes to inline styles
	      da.addStyleSheet(null, CSSNorm.stdStyleSheet(), DOMAnalyzer.Origin.AGENT); //use the standard style sheet
	      da.addStyleSheet(null, CSSNorm.userStyleSheet(), DOMAnalyzer.Origin.AGENT); //use the additional style sheet
	      da.getStyleSheets(); //load the author style sheets

	      //Compute the styles
	      System.err.println("Computing style...");
	      da.stylesToDomInherited();

	      //Save the output
	      PrintStream os = new PrintStream(new FileOutputStream("EmailReport.HTML"));
	      Output out = new NormalOutput(doc);
	      out.dumpTo(os);
	      os.close();

	      docSource.close();
	}

}
*/
